The enclosed will probably be of interest to anyone who wants to run V7
on semi-modern hardware. It comes from Henry Spencer <henry@spsystems.net>

--------
>From decvax!harpo!utah-cs!lepreau Thu Oct 21 09:32:40 1982
Subject: Re: 11/24 and 11/44 printf problems
Newsgroups: net.unix-wizards

I expect this is the solution.  It gave me a hell of a time one night a
year or so ago when some date conversion routines suddenly started
breaking.  This one is worth saving... it is wrong in 2.8 bsd too. Kudos
to Henry and the folks in Canada.
-Jay Lepreau

>From harpo!mhtsa!ihnss!ucbvax!decvax!utzoo!henry Tue Dec  1 06:46:23 1981
Subject: ldiv bug fix
Newsgroups: net.v7bugs
The ldiv bug reported by watmath!egisin seems to be yet another bit of
bad behavior in ldiv that is fixed by the div-abort bugfix I put out on
net.v7bugs in May.  At least, his example works fine here.

>From randvax!decvax!utzoo!henry  Fri Dec 11 04:37:29 1981
Subject: ldiv bug

Following are the two articles I sent describing the bug and the fix.
It also seems to fix yet another bug:  (unsigned)32768/1 supposedly
used to screw up.

						Henry Spencer

Autzoo.643
NET.v7bugs
utzoo!henry
Thu May 21 20:22:51 1981
ldiv/lrem on 44,23
The V7 long-int divide and remainder routines, as distributed by Bell,
make an invalid assumption about the DIV instruction on the 11.  DIV
aborts if the quotient is too big for a signed 16-bit number;  the
routines assume that the dividend register pair is untouched afterwards.
This was generally true on early 11's, but Dec has never guaranteed it
and it is NOT TRUE on the 11/44.  111111111 [that's nine 1's] % 10 yields 11.

The fix is fairly easy;  it's the same fix for all six occurrences: the 
libc ldiv, lrem, aldiv, and alrem, and the kernel ldiv and lrem in mch.s .
Look for a DIV followed by a BVC.  If the BVC falls through, r0 and r1
must be put back as they were before the DIV.  Specifically:

	1. Before DIV, add "mov r0,-(sp)".
	2. After BVC, add "mov r2,r1" and "mov (sp),r0".
	3. After label "1", about 6 lines down, add "tst (sp)+".

The fixed-up routines function properly whether the particular cpu manifests
the problem or not, so this fix can be universally applied.

This also may have cured the largest-negative-dividend bug that the
V7-addenda-tape README alludes to;  at least, I can't reproduce said bug.

Another local installation has discovered that similar divide anomalies
occur on the 11/23 and can be cured with the same fixes.  I do not have
a test case for the 23;  111111111%10 works fine on it.

Autzoo.681
NET.v7bugs
utzoo!henry
Tue Jun  2 16:08:27 1981
ldiv/lrem on 23, ch.2
Addendum to my previous NET.v7bugs message about the ldiv/lrem bug on
the 44 and 23:  the people who discovered that the problem also occurs
on the 23 have supplied test cases.  If you want to see the problem on
a 23, try  11335500/100  or  11335577%100  .  Thanks to Chris Sturgess
and Ron Gomes of Human Computing Resources for this one.

--------
