head	1.2;
access;
symbols;
locks
	mhol:1.2; strict;
comment	@# @;


1.2
date	2002.02.04.22.23.09;	author mhol;	state Exp;
branches;
next	1.1;

1.1
date	2001.09.27.05.27.20;	author mhol;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<!--Converted with LaTeX2HTML 99.2beta6 (1.42)
original version by:  Nikos Drakos, CBLU, University of Leeds
* revised and updated by:  Marcus Hennecke, Ross Moore, Herb Swan
* with significant contributions from:
  Jens Lippmann, Marek Rouchal, Martin Wilck and others -->
<HTML>
<HEAD>
<TITLE>Unix Archive Sites</TITLE>
<META NAME="description" CONTENT="Unix Archive Sites">
<META NAME="keywords" CONTENT="archive_sites">
<META NAME="resource-type" CONTENT="document">
<META NAME="distribution" CONTENT="global">

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<META NAME="Generator" CONTENT="LaTeX2HTML v99.2beta6">
</HEAD>

<BODY BGCOLOR="#ffffff">
<BR> <P>

<!--End of Navigation Panel-->

<P>

<H1><A NAME="SECTION00010000000000000000">
Unix Archive Sites</A>
</H1>

<P>
The Unix Archive is available via various protocols from several sites.
We would love to have more mirrors of this archive. If you could provide
a mirror of this site, please let <A NAME="tex2html1"
  HREF="mailto:wkt@@tuhs.org">Warren Toomey</A>
know.

<p>
<table>
<tr><td align=center><b>FTP Access</b></td><td align=center><b>Web Access</b></td></tr>
<tr><td valign=top>
FTP access to the Unix Archive is available from:&nbsp;&nbsp;
<P>
<UL>
<li><a href="ftp://ftp.lug.udel.edu/pub/oldunix">ftp.lug.udel.edu</a> in the US.
<li><a href="ftp://ftp.tux.org/sites/minnie.tuhs.org/">ftp.tux.org</a> in the US.
<li><a href="ftp://sunsite.icm.edu.pl/pub/unix/UnixArchive/">sunsite.icm.edu.pl</a> in Poland.
<li><a href="ftp://ftp.openbsd.ru/pub/unix-archive/">ftp.openbsd.ru</a> in Russia.
<li><a href="ftp://ftp.darwinsys.com/pub/UnixArchive">ftp.darwinsys.com</a> in Canada.
<li><a href="ftp://ftp.ics.es.osaka-u.ac.jp/pub/mirrors/UnixArchive/">ftp.ics.es.osaka-u.ac.jp</a> in Japan.
<li><a href="ftp://ftp.cs.tu-berlin.de/pub/bsd/UnixArchive/">ftp.cs.tu-berlin.de</a> in Germany.
<li><a href="ftp://mirror.interoute.net/pub/UnixArchive/">mirror.interoute.net</a> in England.
<LI><a href="ftp://ftp.mirrors.wiretapped.net/pub/UnixArchive/">ftp.mirrors.wiretapped.net</a> in Australia.
<LI><a href="ftp://minnie.tuhs.org/UnixArchive">minnie.tuhs.org</a> in Australia.
<li><a href="ftp://ftp.win.tue.nl/pub/home/aeb/unix-archive/">ftp.win.tue.nl</a> in the Netherlands.
<li><a href="ftp://ftp.tuhs.org.ua">ftp.tuhs.org.ua</a> in the Ukraine.
</LI>
</UL>
</td>
<td valign=top>
Web access to the Unix Archive is available from:
<p>
<UL>
<li><a href="http://ftp.lug.udel.edu/pub/oldunix">ftp.lug.udel.edu</a> in the US.
<li><a href="http://nibbly.york.ac.uk/mirrors/">nibbly.york.ac.uk</a> in England.
<LI><A HREF="http://www.wiretapped.be/UnixArchive">www.wiretapped.be</a> in Belgium.
<LI><A HREF="http://www.mirrors.wiretapped.net/UnixArchive/">www.mirrors.wiretapped.net</a> in Australia.
<LI><A HREF="http://www.tuhs.org/Archive">minnie.tuhs.org</A>
in Australia.
<LI><A HREF="http://unix-archive.pdp11.org.ru">unix-archive.pdp11.org.ru</A>
in Russia.
</LI>
</UL>
</td></tr>
</table>

<P>

<H1><A NAME="SECTION00040000000000000000">
Rsync Access</A>
</H1>

<P>
The Unix Archive can also be accessed via an rsync server on
minnie.tuhs.org, but <i>only if you are a registered mirror site</i>.
See the <A HREF="http://rsync.samba.org/">rsync web page</A>
for details on how to obtain a client.

<P>
The rsync access is divided into a number of categories in order to
ease the load on the Unix Archive server. The categories are:

<P>
<DIV ALIGN="CENTER">
<TABLE CELLPADDING=3 BORDER="1">
<TR><TD ALIGN="CENTER" COLSPAN=1><B>Name</B></TD>
<TD ALIGN="CENTER" COLSPAN=1><B>Holds</B></TD>
<TD ALIGN="CENTER" COLSPAN=1><B>Size (Megs)</B></TD>
</TR>
<TR><TD ALIGN="LEFT">UA_Root</TD>
<TD ALIGN="LEFT">root files</TD>
<TD ALIGN="LEFT">5M</TD>
</TR>
<TR><TD ALIGN="LEFT">UA_Applications</TD>
<TD ALIGN="LEFT">Applications directory</TD>
<TD ALIGN="LEFT">65M</TD>
</TR>
<TR><TD ALIGN="LEFT">UA_4BSD</TD>
<TD ALIGN="LEFT">4BSD directory</TD>
<TD ALIGN="LEFT">371M</TD>
</TR>
<TR><TD ALIGN="LEFT">UA_PDP11</TD>
<TD ALIGN="LEFT">PDP-11 directory</TD>
<TD ALIGN="LEFT">479M</TD>
</TR>
<TR><TD ALIGN="LEFT">UA_PDP11_Trees</TD>
<TD ALIGN="LEFT">PDP-11/Trees directory</TD>
<TD ALIGN="LEFT">97M</TD>
</TR>
<TR><TD ALIGN="LEFT">UA_VAX</TD>
<TD ALIGN="LEFT">VAX directory</TD>
<TD ALIGN="LEFT">5M</TD>
</TR>
<TR><TD ALIGN="LEFT">UA_Other</TD>
<TD ALIGN="LEFT">Other directory</TD>
<TD ALIGN="LEFT">6M</TD>
</TR>
</TABLE>
</DIV>

<P>
You can use rsync to mirror each section. To mirror the complete archive,
you would issue the following commands:

<P>
<PRE>
mkdir Applications 4BSD PDP-11 PDP-11/Trees VAX Other

rsync -avz minnie.tuhs.org::UA_Root .
rsync -avz minnie.tuhs.org::UA_Applications Applications
rsync -avz minnie.tuhs.org::UA_4BSD 4BSD
rsync -avz minnie.tuhs.org::UA_PDP11 PDP-11
rsync -avz minnie.tuhs.org::UA_PDP11_Trees PDP-11/Trees
rsync -avz minnie.tuhs.org::UA_VAX VAX
rsync -avz minnie.tuhs.org::UA_Other Other
</PRE>

<HR>
<!--End of Navigation Panel-->
<ADDRESS>
<I>Warren Toomey <BR>
2001-05-04</I>
</ADDRESS>
</BODY>
</HTML>
@


1.1
log
@Initial revision
@
text
@a17 4
<META HTTP-EQUIV="Content-Style-Type" CONTENT="text/css">

<LINK REL="STYLESHEET" HREF="archive_sites.css">

d38 5
a43 16
All archive sites require that only legitimate holders of a UNIX source
license can access the archive. You will need to request access to the
archive and prove you hold a UNIX source license. Once confirmed, you will
receive a username and password to access the archive.

<P>

<H1><A NAME="SECTION00020000000000000000">
FTP Access</A>
</H1>

<P>
Password-protected FTP access to the Unix Archive is available from:

<P>

d45 12
a56 1
<LI>minnie.tuhs.org in Australia.
d59 4
a62 12

<P>

<H1><A NAME="SECTION00030000000000000000">
Web Access</A>
</H1>

<P>
Password-protected HTTP access to the Unix Archive is available from:

<P>

d64 5
a68 2
<LI><A NAME="tex2html2"
  HREF="http://www.tuhs.org/Archive">minnie.tuhs.org</A>
d70 2
d74 2
d85 2
a86 2
minnie.tuhs.org. See the <A NAME="tex2html3"
  HREF="http://rsync.samba.org/">rsync web page</A>
d102 1
a102 1
<TD ALIGN="LEFT">1M</TD>
d114 5
a118 1
<TD ALIGN="LEFT">492M</TD>
d126 1
a126 1
<TD ALIGN="LEFT">3M</TD>
d137 1
a137 1
mkdir Applications 4BSD PDP-11 VAX Other
d139 7
a145 6
rsync -avz username@@minnie.tuhs.org::UA_Root .
rsync -avz username@@minnie.tuhs.org::UA_Applications Applications
rsync -avz username@@minnie.tuhs.org::UA_4BSD 4BSD
rsync -avz username@@minnie.tuhs.org::UA_PDP11 PDP-11
rsync -avz username@@minnie.tuhs.org::UA_VAX VAX
rsync -avz username@@minnie.tuhs.org::UA_Other Other
a147 3
<P>
where username is the username you were given once your Unix Archive
access was granted.
@
