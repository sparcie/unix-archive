Welcome to this complete Australian Unix Users Group (AUUG) newsletter
collection, spanning over 25 years from October 1978 through to June 2005.
Most of the scanned AUUGNs here come from Warren Toomey's collection,
except for the following:

  - John Dodgson's scans for V01.1 ... V03.6, which came
    from http://www.physiol.usyd.edu.au/johnd/auug

  - Steve Jenkin donated the following issues for scanning:
    V05.2, V05.3, V05.4, V08.5, V11.2, V19.4, V20.3, V20.4,
    V21.2, V21.3, V25.1 ... V25.4

  - Frank Crawford donated the following issues for scanning:
    V09.5, V10.5, V10.6, V11.1, V11.3, V11.4, V12.1, V12.4/5,
    V12.6, V13.1, V13.3, V13.6, V14.1, V14.2

  - David Purdue donated the following issues for scanning:
    V09.2, V09.3, V12.1, V12.2/3, V13.2, V13.4
